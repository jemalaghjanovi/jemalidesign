import java.util.Scanner;

//task 3.7

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Enter an amount in double, for example 11.56: ");
        double amount = input.nextDouble();

        int remainingAmount = (int) (amount * 100);

        int numberOfDollars = remainingAmount / 100;
        remainingAmount = remainingAmount % 100;

        int numberOfQuarters = remainingAmount / 25;
        remainingAmount = remainingAmount % 25;

        int numberOfDimes = remainingAmount / 10;
        remainingAmount = remainingAmount % 10;

        int numberOfNickels = remainingAmount / 5;
        remainingAmount = remainingAmount % 5;

        int numberOfPennies = remainingAmount;

        System.out.println("Your amount " + amount + " consists of");

        if (numberOfDollars > 0) {
            System.out.println(numberOfDollars + (numberOfDollars == 1 ? " dollar" : " dollars"));
        }
        if (numberOfQuarters > 0) {
            System.out.println(numberOfQuarters + (numberOfQuarters == 1 ? " quarter" : " quarters"));
        }
        if (numberOfDimes > 0) {
            System.out.println(numberOfDimes + (numberOfDimes == 1 ? " dime" : " dimes"));
        }
        if (numberOfNickels > 0) {
            System.out.println(numberOfNickels + (numberOfNickels == 1 ? " nickel" : " nickels"));
        }
        if (numberOfPennies > 0) {
            System.out.println(numberOfPennies + (numberOfPennies == 1 ? " penny" : " pennies"));
        }
    }
}
